/*
* Given an integer n, calculate the nth row of Pascal's triangle
* 
* @param n
*	 row number of Pascal's triangle, where the first row is n = 0
* 
* @return 
*	an array of the nth row of Pascal's triangle,
* 	or an empty array if n is not valid
*/
const ptRow = (n) =>
{
    /* your code here */
    if(n<0) return [];

    var result = new Array();

    var i = 0;

    while(i <= n)
    {
        result.push(factorial(n)/(factorial(i)*factorial(n-i)));
        i++;
    }

    console.log(result);

    return result;
}

const factorial = (n) =>
{
    if(n<0) 
        return -1;

    else if (n==0)
        return 1;

    else 
        return (n * factorial(n-1));
}